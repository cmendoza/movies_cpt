(function( $ ) {
	'use strict';

	$(function() {
	
		var importTimer;
		var importingData= 0;
			
		/* Every second, we're going to poll the server to request for the
		 * value of the progress being made. This is using the get_import_status
		 * function on the server-side.
		 *
		 * If the response is -1, then the operation is done and we can stop the
		 * timer; otherwise, we can update the progressbar.
		 */
		importTimer = setInterval(function() {

			// Get the current status of the update
			$.get( ajaxurl, {
				action:    'get_import_csv_status'
			}, function( response ) {

				if ( '-1' === response ) {

					if ($('#csv_import_status').length && importingData > 0){
						$('#csv_import_status').text('Data imported');
						$('#waitmsg').text('');
					}
					clearInterval(importTimer);
				} else if( '1' === response) {
					if( $.trim( $('#csv_import_status').html() ).length > 0) {
						$('#csv_import_status').text('Data imported');
					}
					clearInterval(importTimer);
				} else {
					if ($('#csv_import_status').length){
						$('#csv_import_status').text('Processing data, please wait....');
					}
					importingData = 1;

				}


			});

		}, 4000 );

		$.post( ajaxurl, {

			action: 'import_csv_files'

		}, function( response ) {

			/* You can control the response from the server. In this case,
			 * I'm sending a '0' whenever all of the files have been imported.
			 */
			if ( '0' === response ) {
				if ($('#csv_import_status').length) {
					$('#csv_import_status').text('Processing data, please wait....');
				}
				console.log('response from import_csv_files: '+response);
				// Update the progress to 100%, display an 'import complete' message
				
			}

		});

	});


})( jQuery );
