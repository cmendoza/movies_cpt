<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://onlinevortex.com
 * @since      1.0.0
 *
 * @package    Movies_cpt
 * @subpackage movies_cpt/admin/partials
 */
?>

		<h2>Upload a File</h2>
		<form  method="post" enctype="multipart/form-data">
			<input type='file' id='test_upload_csv' name='test_upload_csv'></input>
			<?php submit_button('Upload') ?>
		</form>
		<div id="csv_import_status"></div>
