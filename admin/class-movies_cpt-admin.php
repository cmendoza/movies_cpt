<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://onlinevortex.com
 * @since      1.0.0
 *
 * @package    Movies_cpt
 * @subpackage movies_cpt/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Movies_cpt
 * @subpackage movies_cpt/admin
 * @author     onlinevortex.com <carlosm@onlinevortex.com>
 */
class Movies_cpt_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Movies_cpt_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Movies_cpt_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/movies_cpt-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Movies_cpt_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Movies_cpt_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/movies_cpt-admin.js', array( 'jquery' ), $this->version, false );

	}


	/**
	 * Creates a new custom post type
	 *
	 * @since 	1.0.0
	 * @access 	public
	 * @uses 	register_post_type()
	 */
	public function new_cpt_movies() {
		$cap_type 	= 'post';
		$plural 	= 'Movies';
		$single 	= 'Movie';
		$cpt_name 	= 'movies';
		$opts['can_export']								= TRUE;
		$opts['capability_type']						= $cap_type;
		$opts['description']							= '';
		$opts['exclude_from_search']					= FALSE;
		$opts['has_archive']							= TRUE;
		$opts['hierarchical']							= FALSE;
		$opts['map_meta_cap']							= TRUE;
		$opts['menu_icon']								= 'dashicons-admin-post';
		$opts['menu_position']							= 25;
		$opts['public']									= TRUE;
		$opts['publicly_querable']						= TRUE;
		$opts['query_var']								= TRUE;
		$opts['register_meta_box_cb']					= '';
		$opts['rewrite']								= FALSE;
		$opts['show_in_admin_bar']						= TRUE;
		$opts['show_in_menu']							= TRUE;
		$opts['show_in_nav_menu']						= TRUE;
		$opts['show_ui']								= TRUE;
		$opts['supports']								= array( 'title', 'editor', 'thumbnail', 'custom-fields' );
		$opts['taxonomies']								= array();
		$opts['capabilities']['delete_others_posts']	= "delete_others_{$cap_type}s";
		$opts['capabilities']['delete_post']			= "delete_{$cap_type}";
		$opts['capabilities']['delete_posts']			= "delete_{$cap_type}s";
		$opts['capabilities']['delete_private_posts']	= "delete_private_{$cap_type}s";
		$opts['capabilities']['delete_published_posts']	= "delete_published_{$cap_type}s";
		$opts['capabilities']['edit_others_posts']		= "edit_others_{$cap_type}s";
		$opts['capabilities']['edit_post']				= "edit_{$cap_type}";
		$opts['capabilities']['edit_posts']				= "edit_{$cap_type}s";
		$opts['capabilities']['edit_private_posts']		= "edit_private_{$cap_type}s";
		$opts['capabilities']['edit_published_posts']	= "edit_published_{$cap_type}s";
		$opts['capabilities']['publish_posts']			= "publish_{$cap_type}s";
		$opts['capabilities']['read_post']				= "read_{$cap_type}";
		$opts['capabilities']['read_private_posts']		= "read_private_{$cap_type}s";
		$opts['labels']['add_new']						= __( "Add New {$single}", 'movies_cpt' );
		$opts['labels']['add_new_item']					= __( "Add New {$single}", 'movies_cpt' );
		$opts['labels']['all_items']					= __( $plural, 'now-hiring' );
		$opts['labels']['edit_item']					= __( "Edit {$single}" , 'movies_cpt' );
		$opts['labels']['menu_name']					= __( $plural, 'nmovies_cpt' );
		$opts['labels']['name']							= __( $plural, 'movies_cpt' );
		$opts['labels']['name_admin_bar']				= __( $single, 'movies_cpt' );
		$opts['labels']['new_item']						= __( "New {$single}", 'movies_cpt' );
		$opts['labels']['not_found']					= __( "No {$plural} Found", 'movies_cpt' );
		$opts['labels']['not_found_in_trash']			= __( "No {$plural} Found in Trash", 'movies_cpt' );
		$opts['labels']['parent_item_colon']			= __( "Parent {$plural} :", 'movies_cpt' );
		$opts['labels']['search_items']					= __( "Search {$plural}", 'movies_cpt' );
		$opts['labels']['singular_name']				= __( $single, 'now-hiring' );
		$opts['labels']['view_item']					= __( "View {$single}", 'movies_cpt' );
		$opts['rewrite']['ep_mask']						= EP_PERMALINK;
		$opts['rewrite']['feeds']						= FALSE;
		$opts['rewrite']['pages']						= TRUE;
		$opts['rewrite']['slug']						= __( strtolower( $plural ), 'movies_cpt' );
		$opts['rewrite']['with_front']					= TRUE;
		$opts['show_in_rest']							= TRUE;
		$opts = apply_filters( 'movies_cpt-cpt-options', $opts );
		register_post_type( strtolower( $cpt_name ), $opts );
	} 
	/**
	 * Creates a new taxonomy for a custom post type
	 *
	 * @since 	1.0.0
	 * @access 	public
	 * @uses 	register_taxonomy()
	 */
	public function new_taxonomy_type() {
		$plural 	= 'Genres';
		$single 	= 'Genre';
		$tax_name 	= 'movie_type';
		$opts['hierarchical']							= TRUE;
		//$opts['meta_box_cb'] 							= '';
		$opts['public']									= TRUE;
		$opts['query_var']								= $tax_name;
		$opts['show_admin_column'] 						= TRUE;
		$opts['show_in_nav_menus']						= TRUE;
		$opts['show_tag_cloud'] 						= TRUE;
		$opts['show_ui']								= TRUE;
		$opts['sort'] 									= '';
		//$opts['update_count_callback'] 					= '';
		$opts['capabilities']['assign_terms'] 			= 'edit_posts';
		$opts['capabilities']['delete_terms'] 			= 'manage_categories';
		$opts['capabilities']['edit_terms'] 			= 'manage_categories';
		$opts['capabilities']['manage_terms'] 			= 'manage_categories';
		$opts['labels']['add_new_item'] 				= __( "Add New {$single}", 'movies_cpt' );
		$opts['labels']['add_or_remove_items'] 			= __( "Add or remove {$plural}", 'movies_cpt' );
		$opts['labels']['all_items'] 					= __( $plural, 'movies_cpt' );
		$opts['labels']['choose_from_most_used'] 		= __( "Choose from most used {$plural}", 'movies_cpt' );
		$opts['labels']['edit_item'] 					= __( "Edit {$single}" , 'movies_cpt');
		$opts['labels']['menu_name'] 					= __( $plural, 'movies_cpt' );
		$opts['labels']['name'] 						= __( $plural, 'movies_cpt' );
		$opts['labels']['new_item_name'] 				= __( "New {$single} Name", 'movies_cpt' );
		$opts['labels']['not_found'] 					= __( "No {$plural} Found", 'movies_cpt' );
		$opts['labels']['parent_item'] 					= __( "Parent {$single}", 'movies_cpt' );
		$opts['labels']['parent_item_colon'] 			= __( "Parent {$single}:", 'movies_cpt' );
		$opts['labels']['popular_items'] 				= __( "Popular {$plural}", 'movies_cpt' );
		$opts['labels']['search_items'] 				= __( "Search {$plural}", 'movies_cpt' );
		$opts['labels']['separate_items_with_commas'] 	= __( "Separate {$plural} with commas", 'movies_cpt' );
		$opts['labels']['singular_name'] 				= __( $single, 'movies_cpt' );
		$opts['labels']['update_item'] 					= __( "Update {$single}", 'movies_cpt' );
		$opts['labels']['view_item'] 					= __( "View {$single}", 'movies_cpt' );
		$opts['rewrite']['ep_mask']						= EP_NONE;
		$opts['rewrite']['hierarchical']				= FALSE;
		$opts['rewrite']['slug']						= __( strtolower( $tax_name ), 'movies_cpt' );
		$opts['rewrite']['with_front']					= FALSE;
		$opts = apply_filters( 'movies_cpt-taxonomy-options', $opts );
		register_taxonomy( $tax_name, 'movies', $opts );
	} 

	/**
	 * Adds custom fields to Movie CPT
	 */
	function add_movies_custom_fields() {
		if(function_exists("register_field_group")) {

			register_field_group(array (
				'id' => 'acf_title-fields',
				'title' => 'Title fields',
				'fields' => array (
					array (
						'key' => 'field_55a85200acff9',
						'label' => 'Pre Title',
						'name' => 'pre_title',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => 'Pre title',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55a8522facffa',
						'label' => 'Post Title',
						'name' => 'post_title',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => 'Post Title',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'movies',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'acf_after_title',
					'layout' => 'no_box',
					'hide_on_screen' => array (
						0 => 'custom_fields',
					),
				),
				'menu_order' => 0,
			));

			register_field_group(array (
				'id' => 'acf_links',
				'title' => 'Links',
				'fields' => array (
					array (
						'key' => 'field_55a85a4089743',
						'label' => 'Trailer link',
						'name' => 'trailer_link',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => 'Trailer link',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55a85a6489744',
						'label' => 'Embed Code',
						'name' => 'embed_url',
						'type' => 'textarea',
						'default_value' => '',
						'placeholder' => 'Embed URL',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55a85a7489745',
						'label' => 'More info',
						'name' => 'more_info',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => 'Link for more info',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'movies',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 1,
			));

			register_field_group(array (
				'id' => 'acf_short-review',
				'title' => 'Short review',
				'fields' => array (
					array (
						'key' => 'field_55a85ae3690dc',
						'label' => 'Short review',
						'name' => 'short_review',
						'type' => 'wysiwyg',
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'no',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => 4,
						'formatting' => 'br',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'movies',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 2,
			));

			register_field_group(array (
				'id' => 'acf_images',
				'title' => 'Images',
				'fields' => array (
					array (
						'key' => 'field_55a85b476dd85',
						'label' => 'Film cover',
						'name' => 'film_cover',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_55a8604040ca6',
						'label' => 'Stills',
						'name' => 'stills',
						'type' => 'gallery',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'movies',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 4,
			));

			register_field_group(array (
				'id' => 'acf_details',
				'title' => 'Details',
				'fields' => array (
					array (
						'key' => 'field_55a8615f010de',
						'label' => 'Director',
						'name' => 'director',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => 'Director name',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55a8616b010df',
						'label' => 'Year',
						'name' => 'year',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'none',
						'maxlength' => 4,
					),
					array (
						'key' => 'field_55a8618e010e0',
						'label' => 'Rating',
						'name' => 'rating',
						'type' => 'image',
						'save_format' => 'object',
						'preview_size' => 'thumbnail',
						'library' => 'all',
					),
					array (
						'key' => 'field_55a8619e010e1',
						'label' => 'Running time',
						'name' => 'running_time',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
//						'append' => 'mins.',
						'formatting' => 'html',
//						'maxlength' => 4,
					),
					array (
						'key' => 'field_55a861ab010e2',
						'label' => 'Country',
						'name' => 'country',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55a861b5010e3',
						'label' => 'Language',
						'name' => 'language',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55b2b135df706',
						'label' => 'Cast',
						'name' => 'cast',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
					array (
						'key' => 'field_55b2b147df707',
						'label' => 'Awards',
						'name' => 'awards',
						'type' => 'text',
						'default_value' => '',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'movies',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 5,
			));


			register_field_group(array (
				'id' => 'acf_short-synopsis',
				'title' => 'short synopsis',
				'fields' => array (
					array (
						'key' => 'field_55b95b5cc892f',
						'label' => 'short synopsis',
						'name' => 'short_synopsis',
						'type' => 'wysiwyg',
						'default_value' => '',
						'toolbar' => 'full',
						'media_upload' => 'no',
						'placeholder' => '',
						'maxlength' => '',
						'rows' => 4,
						'formatting' => 'br',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'movies',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'normal',
					'layout' => 'no_box',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));

			register_field_group(array (
				'id' => 'acf_movies-order',
				'title' => 'Movies order',
				'fields' => array (
					array (
						'key' => 'field_5605b13fdda94',
						'label' => 'Order',
						'name' => 'order_movie',
						'type' => 'text',
						'default_value' => '0',
						'placeholder' => '',
						'prepend' => '',
						'append' => '',
						'formatting' => 'html',
						'maxlength' => '',
					),
				),
				'location' => array (
					array (
						array (
							'param' => 'post_type',
							'operator' => '==',
							'value' => 'movies',
							'order_no' => 0,
							'group_no' => 0,
						),
					),
				),
				'options' => array (
					'position' => 'side',
					'layout' => 'default',
					'hide_on_screen' => array (
					),
				),
				'menu_order' => 0,
			));

		}
	}

	/**
	 * Add mime type to the list of WordPress allowed mime types
	 */
	function add_csv_mime_type($mime_types=array()){
	    $mime_types['csv'] = 'application/csv'; 
	    return $mime_types;
	}
 	
 	/**
 	 * Add 'Import CSV file' option to 'Movies' CPT menu 
 	 */
	function moviescpt_add_import_option() {
	    add_submenu_page('edit.php?post_type=movies', 'Import Movies CSV File', 'Import CSV file', 'edit_posts', 'movies_csv_import', array($this, 'fileupload'));
	}

	/**
	 * Shows the form to upload file
	 */
	function fileupload( $label ) { 
		$this->handle_csv_file_post();
		include( plugin_dir_path( __FILE__). 'partials/csv_file_upload_form.php');
	}

	/**
	 * Handles the uploaded file
	 */
	function handle_csv_file_post(){
		// First check if the file appears on the _FILES array
        if(isset($_FILES['test_upload_csv'])) {
			$pdf = $_FILES['test_upload_csv'];

			$upload = wp_upload_bits($_FILES['test_upload_csv']['name'], null, @file_get_contents($_FILES["test_upload_csv"]['tmp_name']));

			if ( FALSE === $upload['error'] ) {
				echo "<h3>File upload successful!</h3>";
				echo "<p id='waitmsg'>Please wait while data is checked and imported.</p>";
				$this->splitfile($upload['file']);
			} else {
				echo "<h3>Error uploading file: <br/><br/><strong>{$upload['error']}</strong></h3>";
				echo "<p>Please try again with a valid file type.</p>";
			}
		}
	}

	/**
	 * Splits the file for processing
	 */
	function splitfile($file) {

		$upload_dir = wp_upload_dir();

		// Read the contents of the file
		$csv = file_get_contents( $file );

		$csv_rows = explode( "\n", $csv );
		$total_rows = count( $csv_rows );

		// Store the title row. This will be written at the top of every file.
		$title_row = $csv_rows[ 0 ];
		
		$rows_of_ten = floor( $total_rows / 10 );
		$remaining_row = ( $rows_of_ten % 10 );
		
		// Prepare to write out the files. This could just as easily be a for loop.
		$file_counter = 0;
		$csv_data = '';
		while( 0 <= $rows_of_ten ) {
		
			$output_file = fopen( $upload_dir['path']. '/moviescpt_csv_' . $file_counter . '.csv', 'w' );
			
			// Calculate number of loops for this file
			$loops = ( $rows_of_ten == 0 && $remaining_row != 0 ) ? $remaining_row + 1 : 10 ;
			
			// Write out the title and then a batch of 10 rows into a string.
			for( $i = 0; $i <= $loops; $i++ ) {

				if (isset($csv_rows[$i])) {
					$csv_data .= $csv_rows[ $i ] . "\n";
					unset( $csv_rows[ $i ] );
				}
			}

			fwrite( $output_file, $csv_data );
			fclose( $output_file );

			$file_counter++;
			$rows_of_ten--;
			$csv_rows = array_values( $csv_rows );
			$csv_data = $title_row . "\n";
		}
		update_option( 'moviescpt-number-of-files', $file_counter );
		update_option( 'moviescpt-parsed-files', 0 );
	}

	/**
	 * Imports the data from CSV files
	 */
	public function import_csv_files() {
		$upload_dir = wp_upload_dir();

		if ( FALSE === ( $parsed_files = get_option( 'moviescpt-parsed-files' ) ) ) {
			$file_to_parse = 0;
		} else {
			$file_to_parse = $parsed_files;			
		}

		if (file_exists( $upload_dir['path']. '/moviescpt_csv_' . $file_to_parse . '.csv' ) ) {

			// Set flag to indicate we are processing data
			update_option( 'moviescpt-importing-data', 1 );

			$row = 1;
			if (($handle = fopen($upload_dir['path']. '/moviescpt_csv_' . $file_to_parse . '.csv', "r")) !== FALSE) {
			    while (($data = fgetcsv($handle, 4000, ",")) !== FALSE) {
					$num = count($data);
					if ( $row == 1 ) {
						$headers = $data;
					} else {
						for ($c=0; $c < $num; $c++) {
							$line[$headers[$c]] = $data[$c];
						}

						$line_categories = $this->process_categories($line['Genre']);

						$movie_title = trim($line['B2B Title']);
						if (empty($movie_title)) {
							$movie_title = $line['DisplayTitle'];
						}
						$movie_post = array(
							'post_title'    => wp_strip_all_tags( $movie_title ),
							'post_content'  => $line['Syn'],
							'post_status'   => 'draft',
							'post_author'   => 1,
							'post_type'	  => 'movies',
							'ping_status'   => 'closed' 
						);

						$post_id = wp_insert_post( $movie_post );

						$cast = $line['Cast1'];

						for ($c=2; $c<=6; $c++) {
							$cast_val = trim($line['Cast'.$c]);
							if (!empty( $cast_val )) {
								$cast .= ", ".$line['Cast'.$c];
							}
						}

						if ($post_id > 0 ) {
							update_field( 'field_55a8616b010df', $line['Year'], $post_id );
							update_field( 'field_55a8619e010e1', intval($line['TRT']), $post_id);
							update_field( 'field_55b95b5cc892f', $line['Syn <240'], $post_id);
							update_field( 'field_55a861ab010e2', $line['Country'], $post_id);
							update_field( 'field_55a8615f010de', $line['Director'], $post_id);
							update_field( 'field_55a861b5010e3', $line['Language'], $post_id);
							update_field( 'field_55a85a4089743', $line['TrailerLink'], $post_id);
							update_field( 'field_55b2b135df706', $cast, $post_id);
							update_field( 'field_55b2b147df707', $line['IMPORT!  NOT COMPLETE Awards'], $post_id);
							update_field( 'field_55a85ae3690dc', $line['ReviewsEtc'], $post_id);

							wp_set_object_terms($post_id, $line_categories, 'movie_type', true);
						}

				    }
			        $row++;
			    }
			    fclose($handle);

			}

			unlink ($upload_dir['path']. '/moviescpt_csv_' . $file_to_parse . '.csv');
		}

		$parsed_files = ++$file_to_parse;
		update_option( 'moviescpt-parsed-files', $parsed_files );

		delete_option( 'moviescpt-importing-data' );

		wp_die( 'importing csv' );
	}

	/**
	 * outputs the status to update the admin page
	 */
	public function get_import_csv_status() {

		if ( FALSE == get_option('moviescpt-number-of-files')) {
			wp_die('1');
		}
		if ( FALSE !== get_option( 'moviescpt-parsed-files' ) ) {
	   	
			$parsed_files = floatval( get_option( 'moviescpt-parsed-files' ) );
			$total_files =  floatval( get_option( 'moviescpt-number-of-files' ) );
			if ($total_files > 0) {
				$progress =  $parsed_files / $total_files;
			}

			if ($parsed_files < $total_files) {

				if ( FALSE === get_option( 'moviescpt-importing-data' ) &&  FALSE !== get_option( 'moviescpt-number-of-files' )) {
					$this->import_csv_files();
				}
				wp_die($progress);
			} else {
				delete_option( 'moviescpt-parsed-files' );
				delete_option( 'moviescpt-number-of-files' );
			}
		} else {
			wp_die( '-1' );
		}
	}

	/**
	 * Processes categories in imported data
	 */
	function process_categories($categories) {
		$separator_slash = strpos($categories, "/");
		if ($separator_slash === false) {
			$strings = explode(",", $categories);
		} else {
			$strings = explode("/", $categories);
		}
		if (is_array($strings)) {
			foreach ($strings as $name) {
				$cat_ids[] = $this->add_category($name);
			}
		}
		return $cat_ids;
	}

	/**
	 * Checks if category exists and adds it if it doesn't
	 */
	function add_category($name) {
		$name = trim(str_replace('&', 'and', $name));
		$category_check = get_term_by('name', $name, 'movie_type');
		if (FALSE === $category_check) {
			$name_slug = sanitize_title( $name );
			$cat_array = array(
			  'cat_name' => $name,
			  'category_nicename' => $name_slug,
			  'taxonomy' => 'movie_type' );
			$category_id = wp_insert_category( $cat_array, TRUE );
		} else {
			$category_id = $category_check->term_taxonomy_id;
		}
		return intval($category_id);
	}

	/**
	 * registers required plugin
	 * (TGM) http://tgmpluginactivation.com/
	 */
	function moviescpt_register_required_plugins() {
		$plugins = array(

			array(
				'name'      => 'Responsive Lightbox by dFactory',
				'slug'      => 'responsive-lightbox',
				'required'  => true,
			),

		);

		$config = array(
			'id'           => 'tgmpa-default',                 // Unique ID for hashing notices for multiple instances of TGMPA.
			'default_path' => '',                      // Default absolute path to bundled plugins.
			'menu'         => 'tgmpa-install-plugins', // Menu slug.
			'parent_slug'  => 'themes.php',            // Parent menu slug.
			'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
			'has_notices'  => true,                    // Show admin notices or not.
			'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
			'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
			'is_automatic' => false,                   // Automatically activate plugins after installation or not.
			'message'      => '',                      // Message to output right before the plugins table.

		);

		tgmpa( $plugins, $config );

		}

}
