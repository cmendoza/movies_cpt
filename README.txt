=== Movies CPT ===
Contributors: carlosmendoza
Donate link: 
Tags: CPT, movies
Requires at least: 3.0.1
Tested up to: 4.3
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Adds a 'movies' custom post type and shortcodes to output the data

== Description ==

Adds a 'movies' custom post type with fields for Director, cast, year, awards, movie poster, stills, etc.

Includes shortcodes to display the data on templates, the output is configurable with shortcode parameters

== Installation ==

1. Upload `movies_cpt.php` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

