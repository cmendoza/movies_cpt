<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://onlinevortex.com
 * @since      1.0.0
 *
 * @package    Movies_cpt
 * @subpackage movies_cpt/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Movies_cpt
 * @subpackage movies_cpt/public
 * @author     onlinevortex.com <carlosm@onlinevortex.com>
 */
class Movies_cpt_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		$cpt = get_post_type(get_queried_object_id());
	    if ('movies' == $cpt ) {
	    	// Default style for CPT
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/movies_cpt-public.css', array(), $this->version, 'all' );
			// Fluidbox CSS
			wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/fluidbox.css', array(), $this->version, 'all' );
		}
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		$cpt = get_post_type(get_queried_object_id());
		if ('movies' == $cpt ) {
			// Default js code for CPT
			wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/movies_cpt-public.js', array( 'jquery' ), $this->version, true );
		}
	}

	/**
	 *  Check if the theme has a custom template for single movie, 
	 * if not it uses the one included with the plugin 
	 */
	function movies_single_template($template) {
		$cpt = get_post_type(get_queried_object_id());
	    if ('movies' == $cpt ) {
			$single_postType_template = locate_template("single-{$cpt}.php");
			if( file_exists( $single_postType_template ) ) {
				return $single_postType_template;
			} else {
				$template = dirname( __FILE__ ) . '/templates/single-'.$cpt.'-template.php';
			}
		}
		return $template;
	}

	/**
	 * Checks if the theme has a custom template for the CPT archive, 
	 * if not it uses the one included with the plugin 
	 */
	function movies_archive_template( $archive_template ) {
	     global $post;

	     if ( is_post_type_archive ( 'movies' ) ) {
			$archive_postType_template = locate_template("archive-movies.php");
			if( file_exists( $archive_postType_template ) ) {
				return $archive_postType_template;
			} else {
				$archive_template = dirname( __FILE__ ) . '/templates/movies-archive-template.php';
			}
	     }
	     return $archive_template;
	}

	/**
	 * Checks if the theme has a template for the CPT search results,
	 * if not it uses the one included with the plugin 
	 */
	function movies_search_template($template) {    
		global $wp_query;
		$post_type = get_query_var('post_type');
		if ( $wp_query->is_search && $post_type == 'movies' ) {
			$movies_search_template = locate_template("movies-search.php");
			if (file_exists($movies_search_template)) {
				return $movies_search_template;
			} else {
				$template = dirname( __FILE__ ) . '/templates/movies-search-template.php';
			}
		}
		return $template;
	}

	/**
	 * Modify the search query to search the fields of the CPT
	 */
	function movies_search_join( $join ) {
	    global $wpdb;

	    if ( is_search() ) {
			if ( 'movies' != get_query_var( 'post_type' ) ) {
				return $join;
			}

	        $join .=' LEFT JOIN '.$wpdb->postmeta. ' ON '. $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	    }
	    
	    return $join;
	}

	/**
	 * Modify the search query with posts_where
	 */
	function movies_search_where( $where ) {
	    global $pagenow, $wpdb;
	   
	    if ( is_search() ) {
			if ( 'movies' != get_query_var( 'post_type' ) ) {
				return $where;
			}

	        $where = preg_replace(
	            "/\(\s*".$wpdb->posts.".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
	            "(".$wpdb->posts.".post_title LIKE $1) OR (".$wpdb->postmeta.".meta_value LIKE $1)", $where );
	    }

	    return $where;
	}

	/**
	 * Add distinct clause to eliminate duplicates
	 */
	function movies_search_distinct( $where ) {
	    global $wpdb;

	    if ( is_search() ) {
			if ( 'movies' != get_query_var( 'post_type' ) ) {
				return $where;
			}
	        return "DISTINCT";
	    }

	    return $where;
	}

	/**
	 * Shortcode to show the full title in the template
	 * output can be the raw value or inside a tag
	 */
	public function moviescpt_full_title($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'h1',
			'container-class' => 'fullTitle',
		), $atts );

		$full_title = '<span class="pre_title">'.get_field('pre_title').'</span> <span class="movie_title">'.get_the_title().'</span> <span class="post_title">'.get_field('post_title').'</span>';
		if ($a['type'] == 'tag') {
			$tag = "<{$a['container']} class='{$a['container-class']}'>{$full_title}</{$a['container']}>";
			return $tag;
		} else {
			return $full_title;
		}
	}

	/**
	 * Shortcode to show the pre title in the template
	 */
	function moviescpt_pre_title() {
		$movie_pre_title = get_field('pre_title');
		return '<span class="pre_title">'.$movie_pre_title.'</span>';
	}

	/**
	 * Shortcode to show the post title in the template
	 */
	function moviescpt_post_title() {
		$movie_post_title = get_field('post_title');
		return '<span class="post_title">'.$movie_post_title.'</span>';
	}

	/**
	 * Shortcode to show the trailer link in the template
 	 * output can be the raw value or inside a tag that can be defined as a parameter
	 */
	function moviescpt_trailer_link($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'p',
			'container-class' => 'trailerLink',
			'label' => 'Watch the trailer',
		), $atts );
		$movie_trailer_link = trim(get_field('trailer_link'));
		if (!empty($movie_trailer_link)) {
			if ($a['type'] == 'tag') {
				$tag = "<{$a['container']} class='{$a['container-class']}'><a rel='lightbox' href='{$movie_trailer_link}'><img src='".plugin_dir_url( __FILE__ )."img/PLAY-TRAILER-button.png' class='trailer_img' width='179' height='60' title='{$a['label']}' alt='{$a['label']}' /></a></{$a['container']}>";
				return $tag;
			} else {
				return $movie_trailer_link;
			}
		}
	}

	/**
	 * Shortcode to show the movie/video embed code in the template
 	 * output can be the raw value or inside a tag that can be defined as a parameter
	 */
	function moviescpt_embed_url($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'p',
			'container-class' => 'movie',
		), $atts );
		$movie_embed_url = get_field('embed_url');
		if ($a['type'] == 'tag') {
			$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_embed_url}</{$a['container']}>";
			return $tag;
		} else {
			return $movie_embed_url;
		}
	}

	/**
	 * Shortcode to show a link for more information of the movie/video in the template
 	 * output can be the raw value or inside a tag that can be defined as a parameter
	 */
	function moviescpt_more_info($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'p',
			'container-class' => 'moreInfo',
			'label-class' => 'labelMoreInfo',
			'label' => 'More info',
		), $atts );
		$movie_more_info = get_field('more_info');
		if ($a['type'] == 'tag') {
			$tag = "<{$a['container']} class='{$a['container-class']}'><a href='{$movie_more_info}'><span class='{$a['label-class']}'>{$a['label']}</span></a></{$a['container']}>";
			return $tag;
		} else {
			return $movie_more_info;
		}
	}

	/**
	 * Shortcode to show an image of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * type, size, class, etc...
	 */
	function moviescpt_film_cover($atts) {
		$movie_film_cover = get_field('film_cover');
		if (empty($movie_film_cover)) {
			return;
		}

		$a = shortcode_atts( array(
			'size' => 'thumbnail',
			'type' => 'url',
			'class' => 'img-responsive',
			'alt' => $movie_film_cover['alt'],
			'title' => $movie_film_cover['title'],
			'rel'	=> ''
		), $atts );

		switch ($a['size']) {
			case 'large':
				$img_src = $movie_film_cover['sizes']['large'];
				$img_width = $movie_film_cover['sizes']['large-width'];
				$img_height = $movie_film_cover['sizes']['large-height'];
				break;
			
			case 'medium':
				$img_src = $movie_film_cover['sizes']['medium'];
				$img_width = $movie_film_cover['sizes']['medium-width'];
				$img_height = $movie_film_cover['sizes']['medium-height'];
				break;

			case 'thumbnail':
				$img_src = $movie_film_cover['sizes']['thumbnail'];
				$img_width = $movie_film_cover['sizes']['thumbnail-width'];
				$img_height = $movie_film_cover['sizes']['thumbnail-height'];
				break;

			default:
				$img_src = $movie_film_cover['sizes']['thumbnail'];
				$img_width = $movie_film_cover['sizes']['thumbnail-width'];
				$img_height = $movie_film_cover['sizes']['thumbnail-height'];
				break;
		}
		$img_src_full = $movie_film_cover['sizes']['large'];
		if ($a['type'] == 'url') {
			return $img_src;
		} else {
			if ($a['rel']) {
			$img_tag =  "<a class='movie_poster' href='{$img_src_full}' rel='lightbox'>"
						."<img class='{$a['class']}' alt='{$a['alt']}' rel='{$a['rel']}' title='{$a['title']}' src='{$img_src}?rel={$a['rel']}' width='{$img_width}' height='{$img_height}' />"
						."</a>";
			} else {
			$img_tag =  "<a class='movie_poster' href='{$img_src_full}'>"
						."<img class='{$a['class']}' alt='{$a['alt']}' title='{$a['title']}' src='{$img_src}' width='{$img_width}' height='{$img_height}' />"
						."</a>";
			}
		}
		return $img_tag;
	}

	/**
	 * Shortcode to show additional images of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * type, size, class, etc...
	 */
	function moviescpt_stills($atts) {
		$movie_stills_string = '';
		$movie_stills = get_field('stills');

		$a = shortcode_atts( array(
			'size' => 'thumbnail',
			'type' => 'url',
			'class' => 'movieStill img-responsive',
			'container' => 'div',
			'container-class' => '',
		), $atts );

		if (is_array($movie_stills)) {
			foreach ($movie_stills as $still) {
				$img_src_full = $still['sizes']['post-thumbnail'];
				$img_src = $still['sizes'][$a['size']];
				$img_width = $still['sizes'][$a['size'].'-width'];
				$img_height = $still['sizes'][$a['size'].'-height'];
				if ($a['type'] == 'url') {
					$movie_stills_string .= "<{$a['container']}>".$img_src."</{$a['container']}>";
				} else {
					$movie_stills_string .=  "<{$a['container']}>"
											."<a class='movie_still' href='{$img_src_full}' rel='lightbox'>"
											."<img class='{$a['class']}' alt='{$still['alt']}' title='{$still['title']}' src='{$img_src}' width='{$img_width}' height='{$img_height}' />"
											."</a>"
											."</{$a['container']}>";
				}
			}
		}
		return $movie_stills_string;
	}

	/**
	 * Shortcode to show the short synopsis of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * tag, class, etc...
	 */
	function moviescpt_short_synopsis($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'div',
			'container-class' => 'shortSynopsis',
			'label-class' => 'labelShortSynopsis',
			'label'	=> '',
			'limit' => 80,
		), $atts );
		$movie_short_synopsis = get_field('short_synopsis');
		if (str_word_count($movie_short_synopsis, 0) > $a['limit']) {
			$words = str_word_count($movie_short_synopsis, 2);
			$pos = array_keys($words);
			$movie_short_synopsis = substr($movie_short_synopsis, 0, $pos[$a['limit']]) . '... Read more';
		}
		if ($a['type'] == 'tag') {
			if ($a['label']) {
				$tag = "<{$a['container']} class='{$a['container-class']}'><span class='{$a['label-class']}'>{$a['label']}</span>{$movie_short_synopsis}</{$a['container']}>";
			} else {
				$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_short_synopsis}</{$a['container']}>";
			}
			return $tag;
		} else {
			return $movie_short_synopsis;
		}
	}

	/**
	 * Shortcode to show a short review of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * type, size, class, etc...
	 */
	function moviescpt_short_review($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'div',
			'container-class' => 'shortReview',
			'label-class' => 'labelShortReview',
			'label' => '',
		), $atts );
		$movie_short_review = get_field('short_review');
		if ($a['type'] == 'tag') {
			if ($a['label']) {
				$tag = "<{$a['container']} class='{$a['container-class']}'><span class='{$a['label-class']}'>{$a['label']}</span>{$movie_short_review}</{$a['container']}>";
			} else {
				$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_short_review}</{$a['container']}>";
			}
			return $tag;
		} else {
			return $movie_short_review;
		}
	}

	/**
	 * Shortcode to show the director of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * tag, class, etc...
	 */
	function moviescpt_director($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'h3',
			'container-class' => 'director',
			'label-class' => 'labelDirector',
			'label' => '',
		), $atts );
		$movie_director = trim(get_field('director'));
		if (!empty($movie_director)) {
			if ($a['type'] == 'tag') {
				if ($a['label']) {
					$tag = "<{$a['container']} class='{$a['container-class']}'><span class='{$a['label-class']}'>{$a['label']}</span>{$movie_director}</{$a['container']}>";
				} else {
					$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_director}</{$a['container']}>";
				}
				return $tag;
			} else {
				return $movie_director;			
			}
		}
	}

	/**
	 * Shortcode to show the year the movie/video was made in the template
 	 * the output can be defined with the shortcode parameters
 	 * tag, class, etc...
	 */
	function moviescpt_year($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'p',
			'container-class' => 'year',
			'label-class' => 'labelYear',
			'label' => '',
		), $atts );
		$movie_year = get_field('year');
		if ($a['type'] == 'tag') {
			if ($a['label']) {
				$tag = "<{$a['container']} class='{$a['container-class']}'><span class='{$a['label-class']}'>{$a['label']}</span>{$movie_year}</{$a['container']}>";
			} else {
				$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_year}</{$a['container']}>";
			}
			return $tag;
		} else {
			return $movie_year;
		}
	}

	/**
	 * Shortcode to show the rating image of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * type, size, class, etc...
	 */
	function moviescpt_rating($atts) {
		$movie_rating = get_field('rating');

		$a = shortcode_atts( array(
			'size' => 'thumbnail',
			'type' => 'url',
			'class' => 'ratingImage',
			'alt' => $movie_rating['alt'],
			'title' => $movie_rating['title']
		), $atts );

		switch ($a['size']) {
			case 'large':
				$img_src = $movie_rating['sizes']['large'];
				$img_width = $movie_rating['sizes']['large-width'];
				$img_height = $movie_rating['sizes']['large-height'];
				break;
			
			case 'medium':
				$img_src = $movie_rating['sizes']['medium'];
				$img_width = $movie_rating['sizes']['medium-width'];
				$img_height = $movie_rating['sizes']['medium-height'];
				break;

			case 'thumbnail':
				$img_src = $movie_rating['sizes']['thumbnail'];
				$img_width = $movie_rating['sizes']['thumbnail-width'];
				$img_height = $movie_rating['sizes']['thumbnail-height'];
				break;

			default:
				$img_src = $movie_rating['sizes']['thumbnail'];
				$img_width = $movie_rating['sizes']['thumbnail-width'];
				$img_height = $movie_rating['sizes']['thumbnail-height'];
				break;
		}
		if ($a['type'] == 'url') {
			return $img_src;
		} else {
			$img_tag = "<img class='{$a['class']}' alt='{$a['alt']}' title='{$a['title']}' src='{$img_src}' width='{$img_width}' height='{$img_height}' />";
		}
		return $img_tag;
	}

	/**
	 * Shortcode to show the running time of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * tag, class, etc...
	 */
	function moviescpt_running_time($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'p',
			'container-class' => 'runningTime',
			'label-class' => 'labelRunningTime',
			'label' => '',
		), $atts );
		$movie_running_time = get_field('running_time');
		if ($a['type'] == 'tag') {
			if ($a['label']) {
				$tag = "<{$a['container']} class='{$a['container-class']}'><span class='{$a['label-class']}'>{$a['label']}</span>{$movie_running_time}</{$a['container']}>";
			} else {
				$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_running_time}</{$a['container']}>";
			}
			return $tag;
		} else {
			return $movie_running_time;
		}
	}

	/**
	 * Shortcode to show the country of origin of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * tag, class, etc...
	 */
	function moviescpt_country($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'p',
			'container-class' => 'country',
			'label-class' => 'labelCountry',
			'label' => '',
		), $atts );
		$movie_country = get_field('country');
		if ($a['type'] == 'tag') {
			if ($a['label']) {
				$tag = "<{$a['container']} class='{$a['container-class']}'><span class='{$a['label-class']}'>{$a['label']}</span>{$movie_country}</{$a['container']}>";
			} else {
				$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_country}</{$a['container']}>";
			}
			return $tag;
		} else {
			return $movie_country;
		}
	}

	/**
	 * Shortcode to show the language of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * tag, class, etc...
	 */
	function moviescpt_language($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'p',
			'container-class' => 'language',
			'label-class' => 'labelLanguage',
			'label' => '',
		), $atts );
		$movie_language = get_field('language');
		if ($a['type'] == 'tag') {
			if ($a['label']) {
				$tag = "<{$a['container']} class='{$a['container-class']}'><span class='{$a['label-class']}'>{$a['label']}</span>{$movie_language}</{$a['container']}>";
			} else {
				$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_language}</{$a['container']}>";
			}
			return $tag;
		} else {
			return $movie_language;
		}
	}

	/**
	 * Shortcode to show the cast of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * tag, class, etc...
	 */
	function moviescpt_cast($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'p',
			'container-class' => 'cast',
			'label-class' => 'labelCast',
			'label' => '',
		), $atts );
		$movie_cast = get_field('cast');
		$cast = explode(", ", $movie_cast);
		$movie_cast_labels = '';
		$num_cast = sizeof($cast);
		foreach ($cast as $key => $value) {
			if ($key < $num_cast-1) {
				$movie_cast_labels .= "<span class='cast'>".trim($value)."</span>, ";
			} else{
				$movie_cast_labels .= "<span class='cast'>".trim($value)."</span> ";
			}
		}
		if ($a['type'] == 'tag') {
			if ($a['label']) {
				$tag = "<{$a['container']} class='{$a['container-class']}'><span class='{$a['label-class']}'>{$a['label']}</span> {$movie_cast_labels}</{$a['container']}>";
			} else {
				$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_cast_labels}</{$a['container']}>";
			}
			return $tag;
		} else {
			return $movie_cast;
		}
	}

	/**
	 * Shortcode to show the awards of the movie/video in the template
 	 * the output can be defined with the shortcode parameters
 	 * tag, class, etc...
	 */
	function moviescpt_awards($atts) {
		$a = shortcode_atts( array(
			'type' => 'tag',
			'container' => 'p',
			'container-class' => 'awards',
			'label-class' => 'labelAwards',
			'label' => '',
		), $atts );
		$movie_awards = get_field('awards');
		if ($a['type'] == 'tag') {
			if ($a['label']) {
				$tag = "<{$a['container']} class='{$a['container-class']}'><span class='{$a['label-class']}'>{$a['label']}</span>{$movie_awards}</{$a['container']}>";
			} else {
				$tag = "<{$a['container']} class='{$a['container-class']}'>{$movie_awards}</{$a['container']}>";
			}
			return $tag;
		} else {
			return $movie_awards;
		}
	}

	/**
 	 * Modifies the 'read more' tag...
	 */
	function moviescpt_excerpt_more( $more ) {
		return '&hellip; <a class="read-more" href="' . get_permalink( get_the_ID() ) . '">' . __( 'More', 'movies_cpt' ) . '</a>';
	}
	
	/**
	 * Length in words for the excerpt
	 */
	function moviescpt_excerpt_length( $length ) {
		return 25;
	}

	/**
	 * Outputs ajax variables
	 */
	function moviescpt_js_variables(){ ?>
	  <script type="text/javascript">
	    var ajaxurl = <?php echo json_encode( admin_url( "admin-ajax.php" ) ); ?>;      
	    var ajaxnonce = <?php echo json_encode( wp_create_nonce( "itr_ajax_nonce" ) ); ?>;
	  </script><?php
	}
}
