<?php
/**
 * Search results template
 * used if the theme does not have a custom template for the CPT search results
 * or as an example to use the shortcodes
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main archive" role="main">
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<div>
					<h3>Search movies</h3>
					<form role="search" action="<?php echo site_url('/'); ?>" method="get" id="searchform">
						<input type="text" name="s" placeholder="Search Movies"/>
						<input type="hidden" name="post_type" value="movies" />
						<input type="submit" alt="Search" value="Search" />
					</form>
				</div>
				<h1>Search results for: <?php echo "$s"; ?></h1>
			</header><!-- .page-header -->

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<div class="film-cover-container">
					<?php 
					$film_cover = do_shortcode('[moviescpt_movie_film_cover type="url" size="medium"]');
					if (!empty($film_cover)) {
					?>
						<a href="<?php the_permalink(); ?>">
							<img src="<?php echo do_shortcode('[moviescpt_movie_film_cover type="url" size="medium"]');?>" class="film-cover" />
						</a>
					<?php
					}
					?>
					</div>
					<?php
					if ( is_single() ) : ?>
						<h1 class="entry-title"><?php echo do_shortcode('[moviescpt_movie_pre_title]');?>
					<?php the_title();?> <?php echo do_shortcode('[moviescpt_movie_post_title]');?></h1>
				<?php
			else : ?>
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php echo do_shortcode('[moviescpt_movie_pre_title]');?><?php the_title();?> <?php echo do_shortcode('[moviescpt_movie_post_title]');?></a></h2>
			<?php
			endif;
		?>
					<?php 
						$director = trim(do_shortcode('[moviescpt_movie_director]'));
						if ($director) {
					?>
					<p class="director">Directed by: <?php echo $director; ?></p>
					<?php } ?>

				</header><!-- .entry-header -->
				
				<div class="entry-content">
					<a href="<?php the_permalink(); ?>">

				<?php 
					$cast = trim(do_shortcode('[moviescpt_movie_cast]'));
					if ($cast) {
				?>
				<p><?php echo $cast; ?></p>
				<?php } ?>
				<?php
				$short_synopsis = do_shortcode('[moviescpt_movie_short_synopsis]');

				if ($short_synopsis) {
					echo "<p>{$short_synopsis}</p>";
				} else {
					the_excerpt();
				}
				?>
					</a>
				</div>
<?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				//get_template_part( 'content', get_post_format() );
?>
			</article>
<?php
			// End the loop.
			endwhile;

			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentyfifteen' ),
				'next_text'          => __( 'Next page', 'twentyfifteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyfifteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

<?php get_footer(); ?>
