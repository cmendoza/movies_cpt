<?php 
/**
 * Single movie template
 * used if the theme does not have a custom template for the CPT single post
 * or as an example to use the shortcodes
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main movie" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>


	<div class="post-thumbnail">
		<?php the_post_thumbnail(); ?>
	</div><!-- .post-thumbnail -->
		<header class="entry-header">
			<h1 class="entry-title"><?php echo do_shortcode('[moviescpt_movie_full_title]');?></h1>
			<?php 
				$director = trim(do_shortcode('[moviescpt_movie_director]'));
				if ($director) {
			?>
			<?php echo $director; ?>
			<?php } ?>
		</header><!-- .entry-header -->
	<div class="entry-left">

		<?php 
		$trailer_link = trim(do_shortcode('[moviescpt_movie_trailer_link]'));
		if ($trailer_link) { ?>
			<?php echo $trailer_link; ?>
		<?php } ?>
		<?php 
		$movie_embed = trim(do_shortcode('[moviescpt_movie_embed_url]'));
		if ($movie_embed) { ?>
			<?php echo $movie_embed; ?>
		<?php } ?>
		<?php 
		$more_info = trim(do_shortcode('[moviescpt_movie_more_info]'));
		if ($more_info) { ?>
			<?php echo $more_info; ?>
		<?php } ?>
			
		<p><?php //echo do_shortcode('[moviescpt_movie_film_cover]');?></p>
		<p><?php echo do_shortcode('[moviescpt_movie_film_cover type="image" class="" size="medium" rel="lightbox"]');?></p>
		<?php //echo do_shortcode('[moviescpt_movie_stills]');?>
		<?php echo do_shortcode('[moviescpt_movie_stills container="div" type="image" size="medium"]');?>
		<?php
			$year = trim(do_shortcode('[moviescpt_movie_year]'));
			if ($year) { ?>
			<?php echo $year; ?>
			<?php } ?>
		<?php 
			$running_time = trim(do_shortcode('[moviescpt_movie_running_time]'));
			if ($running_time) { ?>
			<?php echo $running_time; ?>
		<?php } ?>
		<?php 
			$country = trim(do_shortcode('[moviescpt_movie_country]'));
			if ($country) { ?>
			<?php echo $country; ?>
		<?php } ?>
		<?php 
			$language = trim(do_shortcode('[moviescpt_movie_language]'));
			if ($language) { ?>
			<?php echo $language; ?>
		<?php } ?>
		<?php 
			$rating = trim(do_shortcode('[moviescpt_movie_rating type="url" class="test-class something" size="medium"]'));
			if ($rating) { ?>
			<?php echo "<img class='test-class' width='' height='' src='{$rating}' title='' alt='>'"; ?>
		<?php } ?>
		<?php 
			$cast = trim(do_shortcode('[moviescpt_movie_cast]'));
			if ($cast) { ?>
			<?php echo $cast; ?>
		<?php } ?>
		<?php 
			$awards = trim(do_shortcode('[moviescpt_movie_awards]'));
			if ($awards) { ?>
			<?php echo $awards; ?>
		<?php } ?>

	</div>
	<div class="entry-right">

		<div class="entry-content">
		<?php
			$short_review = trim(do_shortcode('[moviescpt_movie_short_review ]'));
			if ($short_review) {
		?>
		<p><?php echo $short_review?></p>
		<?php } ?>
			<?php
				/* translators: %s: Name of current post */
				the_content( sprintf(
					__( 'Continue reading %s', 'movies_cpt' ),
					the_title( '<span class="screen-reader-text">', '</span>', false )
				) );

				wp_link_pages( array(
					'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'movies_cpt' ) . '</span>',
					'after'       => '</div>',
					'link_before' => '<span>',
					'link_after'  => '</span>',
					'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'movies_cpt' ) . ' </span>%',
					'separator'   => '<span class="screen-reader-text">, </span>',
				) );
			?>

		</div><!-- .entry-content -->
	</div>

	<?php
		// Author bio.
		if ( is_single() && get_the_author_meta( 'description' ) ) :
//			get_template_part( 'author-bio' );
		endif;
	?>

	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'movies_cpt' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
<?php
		// End the loop.
		endwhile;
		?>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>