<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://onlinevortex.com
 * @since             1.0.0
 * @package           Movies_cpt
 *
 * @wordpress-plugin
 * Plugin Name:       Movies CPT
 * Plugin URI:        
 * Description:       Adds a 'movies' custom post type and shortcodes to output the data.
 * Version:           1.0.0
 * Author:            carlosmendoza
 * Author URI:        http://onlinevortex.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       movies_cpt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-movies_cpt-activator.php
 */
function activate_movies_cpt() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-movies_cpt-activator.php';
	Movies_cpt_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-movies_cpt-deactivator.php
 */
function deactivate_movies_cpt() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-movies_cpt-deactivator.php';
	Movies_cpt_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_movies_cpt' );
register_deactivation_hook( __FILE__, 'deactivate_movies_cpt' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-movies_cpt.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_movies_cpt() {
	define( 'ACF_LITE' , false ); // hides the ACF menu in the backend

	// include ACF, the image gallery add-on and TGM
	include_once plugin_dir_path( __FILE__ ) . 'includes/advanced-custom-fields/acf.php';
	include_once plugin_dir_path( __FILE__ ) . 'includes/advanced-custom-fields/add-ons/acf-gallery/acf-gallery.php';
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-tgm-plugin-activation.php';

	$plugin = new Movies_cpt();
	$plugin->run();

}

run_movies_cpt();
