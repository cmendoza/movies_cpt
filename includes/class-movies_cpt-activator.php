<?php

/**
 * Fired during plugin activation
 *
 * @link       http://onlinevortex.com
 * @since      1.0.0
 *
 * @package    Movies_cpt
 * @subpackage movies_cpt/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Movies_cpt
 * @subpackage movies_cpt/includes
 * @author     onlinevortex.com <carlosm@onlinevortex.com>
 */
class Movies_cpt_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
