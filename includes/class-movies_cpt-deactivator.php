<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://onlinevortex.com
 * @since      1.0.0
 *
 * @package    Movies_cpt
 * @subpackage movies_cpt/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Movies_cpt
 * @subpackage movies_cpt/includes
 * @author     onlinevortex.com <carlosm@onlinevortex.com>
 */
class Movies_cpt_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
