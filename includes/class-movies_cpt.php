<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://onlinevortex.com
 * @since      1.0.0
 *
 * @package    Movies_cpt
 * @subpackage movies_cpt/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Movies_cpt
 * @subpackage movies_cpt/includes
 * @author     onlinevortex.com <carlosm@onlinevortex.com>
 */
class Movies_cpt {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      movies_cpt_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'movies_cpt';
		$this->version = '0.0.1';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Movies_cpt_Loader. Orchestrates the hooks of the plugin.
	 * - Movies_cpt_i18n. Defines internationalization functionality.
	 * - Movies_cpt_Admin. Defines all hooks for the admin area.
	 * - Movies_cpt_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-movies_cpt-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-movies_cpt-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-movies_cpt-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-movies_cpt-public.php';

		$this->loader = new Movies_cpt_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Movies_cpt_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Movies_cpt_i18n();
		$plugin_i18n->set_domain( $this->get_plugin_name() );

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Movies_cpt_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'init', $plugin_admin, 	'new_cpt_movies' );
		$this->loader->add_action( 'init', $plugin_admin, 	'new_taxonomy_type' );
		$this->loader->add_action( 'init', $plugin_admin, 	'add_movies_custom_fields' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'moviescpt_add_import_option'); 

		// Import CSV data
		$this->loader->add_action( 'wp_ajax_import_csv_files', $plugin_admin, 'import_csv_files' );
		$this->loader->add_action( 'wp_ajax_get_import_csv_status', $plugin_admin, 'get_import_csv_status' );
		$this->loader->add_filter( 'upload_mimes', $plugin_admin, 'add_csv_mime_type');

		//Required plugins
		$this->loader->add_action( 'tgmpa_register', $plugin_admin, 'moviescpt_register_required_plugins' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Movies_cpt_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_filter('single_template', $plugin_public, 'movies_single_template');
		$this->loader->add_filter('archive_template', $plugin_public, 'movies_archive_template');
		$this->loader->add_filter('template_include', $plugin_public, 'movies_search_template'); 
		$this->loader->add_filter('posts_join', $plugin_public, 'movies_search_join' );
		$this->loader->add_filter('posts_where', $plugin_public, 'movies_search_where' );
		$this->loader->add_filter('posts_distinct', $plugin_public, 'movies_search_distinct' );
		$this->loader->add_filter('excerpt_more', $plugin_public, 'moviescpt_excerpt_more', 99 );
		$this->loader->add_filter('excerpt_length', $plugin_public, 'moviescpt_excerpt_length', 999 );

		// template shortcuts
		$this->loader->add_shortcode('moviescpt_movie_full_title', $plugin_public, 'moviescpt_full_title');
		$this->loader->add_shortcode('moviescpt_movie_pre_title', $plugin_public, 'moviescpt_pre_title');
		$this->loader->add_shortcode('moviescpt_movie_post_title', $plugin_public, 'moviescpt_post_title');
		$this->loader->add_shortcode('moviescpt_movie_trailer_link', $plugin_public, 'moviescpt_trailer_link');
		$this->loader->add_shortcode('moviescpt_movie_embed_url', $plugin_public, 'moviescpt_embed_url');
		$this->loader->add_shortcode('moviescpt_movie_more_info', $plugin_public, 'moviescpt_more_info');
		$this->loader->add_shortcode('moviescpt_movie_film_cover', $plugin_public, 'moviescpt_film_cover');
		$this->loader->add_shortcode('moviescpt_movie_stills', $plugin_public, 'moviescpt_stills');
		$this->loader->add_shortcode('moviescpt_movie_short_synopsis', $plugin_public, 'moviescpt_short_synopsis');
		$this->loader->add_shortcode('moviescpt_movie_short_review', $plugin_public, 'moviescpt_short_review');
		$this->loader->add_shortcode('moviescpt_movie_director', $plugin_public, 'moviescpt_director');
		$this->loader->add_shortcode('moviescpt_movie_year', $plugin_public, 'moviescpt_year');
		$this->loader->add_shortcode('moviescpt_movie_rating', $plugin_public, 'moviescpt_rating');
		$this->loader->add_shortcode('moviescpt_movie_running_time', $plugin_public, 'moviescpt_running_time');
		$this->loader->add_shortcode('moviescpt_movie_country', $plugin_public, 'moviescpt_country');
		$this->loader->add_shortcode('moviescpt_movie_language', $plugin_public, 'moviescpt_language');
		$this->loader->add_shortcode('moviescpt_movie_cast', $plugin_public, 'moviescpt_cast');
		$this->loader->add_shortcode('moviescpt_movie_awards', $plugin_public, 'moviescpt_awards');


		$this->loader->add_action ( 'wp_head', $plugin_public, 'moviescpt_js_variables' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Movies_cpt_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
